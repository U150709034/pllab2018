use strict;
use warnings;

my $file = $ARGV[0];

open IN, `<`, $file or die "Cant open $file: $!\n";

my @lines = <IN>;

close IN;

my @words_counts  = ();
foreach my $line (@lines){
	chomp  $line;
	my @row = split(/\s/,my @line);

	foreach my $word (@row) {
		if(exists my $word_counts($word)){ $word_counts($word) += 1;}
		else {$word_counts($word) = 1;}

	}

}

my @words = keys @words_counts;

foreach my $w (@words) {
	print "$w\t$word_counts($w)\n"}
